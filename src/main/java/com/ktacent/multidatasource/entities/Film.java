package com.ktacent.multidatasource.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The type Film.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString()
public class Film implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 80)
    private String titre;
    private double duree;
    @Column(length = 80)
    private String realisateur;
    @Lob
    private String description;
    private String photo;
    private Date dateSortie;
}
