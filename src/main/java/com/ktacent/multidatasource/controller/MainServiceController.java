package com.ktacent.multidatasource.controller;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@Slf4j
public class MainServiceController {
    private static final String MAIN_SERVICE = "mainService";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/call-microservice")
    @ResponseStatus(HttpStatus.OK)
    @CircuitBreaker(name = MAIN_SERVICE, fallbackMethod = "testFallBack")
    public ResponseEntity<String> callService(@RequestHeader("authorization") String token) {
        log.info("service calling service one");
        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", token);
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://localhost:8081/serviceOne",
                HttpMethod.GET
                , httpEntity, String.class);
    }

    @GetMapping("/call-rate-limit-microservice")
    @ResponseStatus(HttpStatus.OK)
    @RateLimiter(name = MAIN_SERVICE, fallbackMethod = "testFallBack")
    public ResponseEntity<String> callWithRateLimiter() throws InterruptedException {
        log.info("service calling service one");
        Thread.sleep(10000);
        return new ResponseEntity<>("response", HttpStatus.OK);

    }

    private ResponseEntity<String> testFallBack(Exception e) {
        return new ResponseEntity<>("In fallback method", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
