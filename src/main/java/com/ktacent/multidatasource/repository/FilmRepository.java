package com.ktacent.multidatasource.repository;

import com.ktacent.multidatasource.entities.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * The interface Film repository.
 */
@RepositoryRestResource
public interface FilmRepository extends JpaRepository<Film, Long> {
}
